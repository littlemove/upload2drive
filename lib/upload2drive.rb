require_relative "upload2drive/version"

require 'google/api_client'
require 'google/api_client/client_secrets'
require 'google/api_client/auth/file_storage'
require 'google/api_client/auth/installed_app'
require 'logger'

require 'pry'

# Script intended to upload a single file to Google drive from the
# command line.
#
# Most of the setup code is directly extracted from the code examples
# from Google's documentation
#
module Upload2Drive

  class Upload2Drive

    API_VERSION = 'v2'
    CACHED_API_FILE = "drive-#{API_VERSION}.cache"
    CREDENTIAL_STORE_FILE = "#{$0}-oauth2.json"

    attr_accessor :result, :client, :drive

    def initialize(file_path)
      @client, @drive = setup()
      @result = insert_file(file_path)
    end


    private
    # Handles authentication and loading of the API.
    def setup()
      log_file = File.open('drive.log', 'a+')
      log_file.sync = true
      logger = Logger.new(log_file)
      logger.level = Logger::DEBUG

      client = Google::APIClient.new(:application_name => 'upload2Drive',
                                     :application_version => '0.0.0')

      # FileStorage stores auth credentials in a file, so they survive multiple runs
      # of the application. This avoids prompting the user for authorization every
      # time the access token expires, by remembering the refresh token.
      # Note: FileStorage is not suitable for multi-user applications.
      file_storage = Google::APIClient::FileStorage.new(CREDENTIAL_STORE_FILE)
      if file_storage.authorization.nil?
        client_secrets = Google::APIClient::ClientSecrets.load
        # The InstalledAppFlow is a helper class to handle the OAuth 2.0 installed
        # application flow, which ties in with FileStorage to store credentials
        # between runs.
        flow = Google::APIClient::InstalledAppFlow.new(
                                                       :client_id => client_secrets.client_id,
                                                       :client_secret => client_secrets.client_secret,
                                                       :scope => ['https://www.googleapis.com/auth/drive']
                                                       )
        client.authorization = flow.authorize(file_storage)
      else
        client.authorization = file_storage.authorization
      end

      drive = nil
      # Load cached discovered API, if it exists. This prevents retrieving the
      # discovery document on every run, saving a round-trip to API servers.
      if File.exists? CACHED_API_FILE
        File.open(CACHED_API_FILE) do |file|
          drive = Marshal.load(file)
        end
      else
        drive = client.discovered_api('drive', API_VERSION)
        File.open(CACHED_API_FILE, 'w') do |file|
          Marshal.dump(drive, file)
        end
      end

      return client, drive
    end

    # Handles files.insert call to Drive API.
    def insert_file(file_path)
      mime_type = `file -Ib #{file_path}`.split(';')[0]
      # Insert a file
      media = Google::APIClient::UploadIO.new(file_path, mime_type)
      result = @client.execute(
                               :api_method => @drive.files.insert,
                               :media => media,
                               :parameters => {
                                 'uploadType' => 'media'}
                               )
      result
    end

  end

end
